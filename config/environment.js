/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'ornio-blog-frontend',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },
    contentSecurityPolicy : {
      'default-src': "'self' *",
      'script-src': "'self' * 'unsafe-inline' 'unsafe-eval' http://localhost localhost:49152 0.0.0.0:49152",
      'font-src': "'self' * https://fonts.googleapis.com",
      'connect-src': "'self' * http://localhost",
      'img-src': "'self' *",
      'style-src': "'self' * 'unsafe-inline' 'unsafe-eval' http://localhost localhost:49152 0.0.0.0:49152",
      'media-src': "'self' *"
    },
    APP: {
        API_HOST: 'http://localhost/blog-api/public',
        API_PATH: 'api/v1'
    },
    'ember-simple-auth': {
        authorizer: 'authorizer:token',
        crossOriginWhitelist: ['http://localhost'],
        authenticationRoute: 'user.login',
        routeAfterAuthentication: 'dashboard',
        routeIfAlreadyAuthenticated: 'dashboard',
        baseURL: '/'
    },
      'ember-simple-auth-token':{
          serverTokenEndpoint: 'http://localhost/blog-api/public/api/v1/users/authenticate',
          serverTokenRefreshEndpoint: 'http://localhost/blog-api/public/api/v1/users/refresh',
          identificationField: 'email',
          refreshAccessTokens: false,
          timeFactor: 1000,
          refreshLeeway: 15
      }
  };

  if (environment === 'development') {
    ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
