import Ember from 'ember';

export default Ember.Component.extend({
    session: Ember.inject.service(),
    actions: {
        authenticate(){
        	var credentials = this.getProperties('identification', 'password'),
                authenticator = 'authenticator:jwt';

            this.get('session').authenticate(authenticator, credentials);
        }
    }
});
