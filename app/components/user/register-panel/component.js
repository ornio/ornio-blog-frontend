import Ember from 'ember';

const { getOwner } = Ember;

export default Ember.Component.extend({
    store: Ember.inject.service(),
    actions: {
        register(){
            const { name, email, password } = this.getProperties('name', 'email', 'password');
            var user = this.get('store').createRecord('user', {name, email, password});
            user.save().then(() =>{
                this.set('name', '');
                this.set('email', '');
                this.set('password', '');
                Materialize.toast('Registred', 4000);
            });
        }
    }
});
