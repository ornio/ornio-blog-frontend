import Ember from 'ember';
import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from './../config/environment';

import ActiveModelAdapter from 'active-model-adapter';

export default ActiveModelAdapter.extend(DataAdapterMixin,{
    host: config.APP.API_HOST,
    namespace: config.APP.API_PATH,

    authorizer: 'authorizer:token',
    isNewSerializerAPI: true
});
