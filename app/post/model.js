import DS from 'ember-data';

export default DS.Model.extend({
  	title: DS.attr('string'),
    description: DS.attr('string'),
    user: DS.belongsTo('user', {async: true}),
    created_at: DS.attr(),
  	updated_at: DS.attr()
});
