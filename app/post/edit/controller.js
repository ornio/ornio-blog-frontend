import Ember from 'ember';

export default Ember.Controller.extend({
    actions:{
        save_post:function(model){
        	
        	let title = model.get('title');
			let description = model.get('description'); 

        	this.store.find('post', model.id).then(function(post) {	
        	  post.set('title', title);        		
			  post.set('description', description);			  
			  post.save();
			});
			
        }
    }
});
