import Ember from 'ember';

export default Ember.Controller.extend({
    actions:{
        create_post(){
            const { title, description } = this.getProperties('title', 'description');
            var post = this.get('store').createRecord('post', {title, description});
            post.save().then(() =>{
                this.set('title', '');
                this.set('description', '');
                Materialize.toast('Created', 4000);
            });
        }
    }
});
