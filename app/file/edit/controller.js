import Ember from 'ember';

export default Ember.Controller.extend({
    actions:{
        save_file:function(model){
        	
        	let file_name = model.get('file_name');
			
        	this.store.find('file', model.id).then(function(file) {	
        	  file.set('file_name', file_name);        					  
			  file.save();
			});
			
        }
    }
});
