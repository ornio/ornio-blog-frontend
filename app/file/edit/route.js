import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin,{
    model: function(params) {
    	console.log(params.img_id);
        return this.store.findRecord('file', params.img_id);
    }
});
