import DS from 'ember-data';

export default DS.Model.extend({
  	file: DS.attr('string'),
  	file_name: DS.attr('string'),
  	file_size: DS.attr('string')
});
