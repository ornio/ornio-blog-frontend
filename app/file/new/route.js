import Ember from 'ember';

const get = Ember.get;
const set = Ember.set;

export default Ember.Route.extend({
	actions: {
	    uploadImage: function (file) {
	      var file = this.modelFor('file');
	      var image = this.store.createRecord('file', {
	        file: file,
	        file_name: get(file, 'name'),
	        file_size: get(file, 'size')
	      });

	      file.read().then(function (url) {
	        if (get(image, 'url') == null) {
	          set(image, 'url', url);
	        }
	      });

	      file.upload('/api/images/upload').then(function (response) {
	        set(image, 'url', response.headers.Location);
	        return image.save();
	      }, function () {
	        image.rollback();
	      });
	    }
	}
});
