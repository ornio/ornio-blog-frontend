import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('dashboard', {path: '/'});
  this.route('user', function() {
    this.route('login');
    this.route('register');
  });

  this.route('post', function() {
    this.route('new');
    this.route('view', {path: ':post_id/view'});
    this.route('edit', {path: ':post_id/edit'});
  });
  this.route('file', function() {
    this.route('new');
    this.route('view', {path: ':img_id/view'});     
    this.route('edit', {path: ':img_id/edit'});
  });
});

export default Router;
