import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Ember.Route.extend(UnauthenticatedRouteMixin,{
	 actions: {
	 	willTransition: function(transition) {
            //Ember.$('body').addClass('gray');
        },
	 }
});
