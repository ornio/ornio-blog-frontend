import Ember from 'ember';

const { inject: { service }, RSVP } = Ember;

export default Ember.Service.extend({
    session: service('session'),
    store: service(),
    hasCompany: false,

    loadCurrentUser() {
        return new RSVP.Promise((resolve, reject) => {
            const token = this.get('session.data.authenticated.token');
            if (!Ember.isEmpty(token)) {
                return this.get('store').queryRecord('user', {}).then((user) => {
                    this.set('currentUser', user);
                    if(user.get('company') !== undefined){
                        this.set('hasCompany', true);
                    }
                    resolve();
                }, reject);
            } else {
                resolve();
            }
        });
    }
});